package com.cs.gm.service;

import com.flowable.core.common.api.security.SecurityScope;
import com.flowable.indexing.query.builder.FilteredFullTextSearchQueryBuilder;
import com.flowable.indexing.utils.ElasticsearchResultConverter;
import com.flowable.platform.common.Page;
import com.flowable.platform.service.index.WorkIndexService;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.queryparser.flexible.standard.QueryParserUtil;

public class CustomWorkIndexService extends WorkIndexService {

    @Override
    protected <T> Page<T> fetchInstances(String filterId, SecurityScope securityScope, int start, int size,
            String searchText, String index, ElasticsearchResultConverter.ResultMapper<T> jsonMapper) {

        FilteredFullTextSearchQueryBuilder queryBuilder = new FilteredFullTextSearchQueryBuilder(objectMapper);
        queryBuilder.from(start).size(size);

        if (FILTER_COMPLETED.equalsIgnoreCase(filterId)) {
            //We are only interested in processes from the "aProcess" model.
            queryBuilder.existsTerm("endTime").mustTerm("definitionKey","aProcess");
        } else if (FILTER_RUNNING.equalsIgnoreCase(filterId) || FILTER_OPEN.equalsIgnoreCase(filterId)) {
            queryBuilder.notExistsTerm("endTime");
        } else if (FILTER_MINE.equalsIgnoreCase(filterId)) {
            queryBuilder.mustTerm("startUserId", securityScope.getUserId());
            queryBuilder.notExistsTerm("endTime");
        }

        addWorkInstancePermissionTerms(securityScope, queryBuilder);

        if (StringUtils.isNotEmpty(searchText)) {
            // Wildcard includes different languages
            queryBuilder.searchTextAndSemantics("full_text_typeAhead*", QueryParserUtil.escape(searchText));
        }

        queryBuilder.sortDescending("startTime");
        String query = queryBuilder.build();

        return getPageFromQuery(start, size, index, jsonMapper, query, "startTime");
    }

}