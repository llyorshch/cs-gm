package com.cs.gm.service;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flowable.action.api.bot.BaseBotActionResult;
import com.flowable.action.api.bot.BotActionResult;
import com.flowable.action.api.bot.BotService;
import com.flowable.action.api.history.HistoricActionInstance;
import com.flowable.action.api.intents.Intent;
import com.flowable.action.api.repository.ActionDefinition;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableMap;
import com.google.common.net.UrlEscapers;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class AsciiArtBotService implements BotService {

    private final ObjectMapper objectMapper;
    private final RestTemplate restTemplate;

    @Override
    public String getKey() {
        return "get-ascii-art";
    }

    @Override
    public String getName() {
        return "Get ASCII Art";
    }

    @Override
    public String getDescription() {
        return "Get Ascii art";
    }

    @Override
    public BotActionResult invokeBot(HistoricActionInstance actionInstance, ActionDefinition actionDefinition,
            Map<String, Object> payload) {
        String inputText = MoreObjects.firstNonNull((String) payload.get("inputText"),"");
        String escapedText = UrlEscapers.urlFormParameterEscaper().escape(inputText);
        String font = MoreObjects.firstNonNull((String) payload.get("font"),"banner");
        JsonNode botResult = objectMapper.valueToTree(ImmutableMap.of("outputText", getAsciiArt(escapedText, font)));
        return new BaseBotActionResult(botResult, Intent.NOOP);
    }

    private String getAsciiArt(String escapedText, String font) {
        String response = "";
        if (!Strings.isEmpty(escapedText)) {
            String url = "http://artii.herokuapp.com/make?text="+escapedText+"&font="+font;
            response = restTemplate.getForObject(url, String.class);
        }
        return response;
    }

}