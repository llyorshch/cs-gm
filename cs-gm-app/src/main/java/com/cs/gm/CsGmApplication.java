package com.cs.gm;

import com.cs.gm.service.CustomWorkIndexService;
import com.flowable.platform.service.index.WorkIndexService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(exclude = {FreeMarkerAutoConfiguration.class})
public class CsGmApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(CsGmApplication.class, args);
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    // @Bean
    // public WorkIndexService workIndexService() {
    //     return new CustomWorkIndexService();
    // }

}
